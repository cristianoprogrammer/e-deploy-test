//
//  Facade.swift
//  E-Deploy
//
//  Created by Cristiano Douglas on 05/07/17.
//  Copyright © 2017 Cristiano Douglas. All rights reserved.
//

import Foundation

/******************************************************************************/
// MARK: Facade
/******************************************************************************/

/// Application facade
class Facade {
	
	/**************************************************************************/
	
	/*........................................................................*/
	// MARK: Shared Instance
	/*........................................................................*/
	
	static let instance = Facade()
	static func getInstance() -> Facade {
		return instance
	}
	
	/*........................................................................*/
	// MARK: Class functions
	/*........................................................................*/
	
	/**
	Request the list of cities on service and save
	
	- parameter completed: Completed closure
	*/
	func requestCities(completed: @escaping () -> ()) -> Void {
		
		ApiService.instance.citiesList(result: { (cities) in
			
			Cidades.instance.save(list: cities!)
			completed()
			
		}) { (message) in
			
			completed()
			
		}
		
	}
	
	/*........................................................................*/
	/**
	Request the city score
	
	- parameter params: params for request
	- parameter completed: completed closure
	- parameter fail: fail closure
	*/
	func requestCityScore(params : [String : Any]?, completed: @escaping (_ score : String?) -> (),  fail: @escaping (_ message : String?) -> ()) -> Void {
		
		ApiService.instance.cityScore(params: params, result: { (score) in
			completed(score)
		}) { (message) in
			fail(message)
		}
		
	}
	
	/*........................................................................*/
	
	/**************************************************************************/
}
