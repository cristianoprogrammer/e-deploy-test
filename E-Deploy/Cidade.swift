//
//  Cidade.swift
//  E-Deploy
//
//  Created by Cristiano Douglas on 05/07/17.
//  Copyright © 2017 Cristiano Douglas. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

/******************************************************************************/
// MARK: City - Object, Mappable
/******************************************************************************/

/// City model - REALM, ObjectMapper
class Cidade: Object, Mappable {
	
	/**************************************************************************/
	
	// City ID
	dynamic var id = 0
	// City Name
	dynamic var Nome	= ""
	// City State
	dynamic var Estado	= ""
	
	/**************************************************************************/
	
	/*........................................................................*/
	
	// Set the primary key
	override static func primaryKey() -> String? {
		return "id"
	}
	
	/*........................................................................*/
	
	//Impl. of Mappable protocol
	required convenience init?(map: Map) {
		
		self.init()
	}
	
	/*........................................................................*/
	
	// Mapping
	func mapping(map: Map) {
		
		Nome    <- map["Nome"]
		Estado	<- map["Estado"]
	}
	
	/**************************************************************************/
	
}

