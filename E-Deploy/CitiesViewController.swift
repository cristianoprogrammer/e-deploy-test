//
//  CitiesViewController.swift
//  E-Deploy
//
//  Created by Cristiano Douglas on 06/07/17.
//  Copyright © 2017 Cristiano Douglas. All rights reserved.
//

import UIKit
import KVNProgress

/******************************************************************************/
// MARK: CitiesViewController -  UIViewController
/******************************************************************************/
class CitiesViewController: UIViewController {

	/**************************************************************************/
	
	/*........................................................................*/
	// MARK: IBOutlet
	/*........................................................................*/
	
	@IBOutlet weak var tableView : UITableView!
	
	/*........................................................................*/
	// MARK: Var
	/*........................................................................*/
	
	var cities			: [Cidade] = [Cidade]()
	var filteredCities	: [Cidade] = [Cidade]()
	var searchActive	: Bool = false
	var selectedCity	: Cidade = Cidade()
	var cityColor		: UIColor!
	
	/*........................................................................*/
	// MARK: Superclass functions
	/*........................................................................*/
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.cities = Cidades.instance.list
		self.filteredCities = self.cities
		
		self.navigationController?.navigationBar.topItem?.title = ""
	}
	
	/*........................................................................*/
	
	override func viewWillAppear(_ animated: Bool) {
		
		self.title = "Busca"
	}

	/*........................................................................*/
	
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

	/*........................................................................*/
	
	// In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		
		if (segue.identifier == "segueCityDetail") {
			
			let vc = segue.destination as! DetailViewController
			vc.city = self.selectedCity
			vc.cityColor = self.cityColor
		}
    }
	
	/**************************************************************************/
	
}

/******************************************************************************/
// MARK: CitiesViewController -  UITableViewDelegate, UITableViewDataSource*/
/******************************************************************************/
extension CitiesViewController : UITableViewDelegate, UITableViewDataSource {
	
	/*........................................................................*/
	
	func numberOfSections(in tableView: UITableView) -> Int {
		
		return 1
	}
	
	/*........................................................................*/
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		
		return self.filteredCities.count
	}
	
	/*........................................................................*/
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "cityCell", for: indexPath) as! CityTableViewCell
		
		cell.cidade = self.filteredCities[indexPath.row]
		
		return cell
	}
	
	/*........................................................................*/
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		
		self.selectedCity = self.filteredCities[indexPath.row]
		let cell = tableView.cellForRow(at: indexPath) as! CityTableViewCell
		self.cityColor = cell.barColor
		
		self.performSegue(withIdentifier: "segueCityDetail", sender: nil)
	}
	
	/*........................................................................*/
}

/******************************************************************************/
// MARK: CitiesViewController -  UISearchBarDelegate*/
/******************************************************************************/
extension CitiesViewController : UISearchBarDelegate {

	/*........................................................................*/
	
	func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
		searchActive = true;
	}
	
	/*........................................................................*/
	
	func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
		searchActive = false;
	}
	
	/*........................................................................*/
	
	func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
		searchActive = false;
		searchBar.resignFirstResponder()
	}
	
	/*........................................................................*/
	
	func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
		searchActive = false;
		searchBar.resignFirstResponder()
	}
	
	/*........................................................................*/
	
	func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
		
		if (!searchText.isEmpty) {
			self.filteredCities = self.cities.filter({ (cidade : Cidade) -> Bool in
				return (cidade.Nome.lowercased().contains(searchText.lowercased()) || cidade.Estado.lowercased().contains(searchText.lowercased()))
			})
			if(self.filteredCities.count == 0){
				searchActive = false;
			} else {
				searchActive = true;
			}
		} else {
			self.filteredCities = self.cities
		}
		
		self.tableView.reloadData()
			
	}
	
	/*........................................................................*/
}
