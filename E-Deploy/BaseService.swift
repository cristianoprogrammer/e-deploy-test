//
//  BaseService.swift
//  E-Deploy
//
//  Created by Cristiano Douglas on 05/07/17.
//  Copyright © 2017 Cristiano Douglas. All rights reserved.
//

import Foundation
import Alamofire

/******************************************************************************/
// MARK: BaseService
/******************************************************************************/

/// The base service request
class BaseService {
	
	/**************************************************************************/
	
	/*........................................................................*/
	
	/**
	Make a POST request - Json result
	
	- parameter url:     Url of internal service
	- parameter params:  Request Params
	- parameter headers: Request Headers
	- parameter result:  Result callback
	*/
	internal func postJsonResponse(url:String!, params:[String:Any]?, headers:[String:String]?, result: @escaping (_ jsonObject:AnyObject?) -> () ) {
		
		Alamofire.request(url, method: HTTPMethod.post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
			
			print(response.result.value as Any)
			result(response.result.value as AnyObject?)
		}
		
	}
	
	/*........................................................................*/
	
	/**
	Make a POST request - String result
	
	- parameter url:     Url of internal service
	- parameter params:  Request Params
	- parameter headers: Request Headers
	- parameter result:  Result callback
	*/
	internal func postStringResponse(url:String!, params:[String:Any]?, headers:[String:String]?, result: @escaping (_ resultString:String?) -> () ) {
		
		Alamofire.request(url, method: HTTPMethod.post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseString { (response) in
			
			print(response.result.value)
			if response.response?.statusCode == 200 {
				result(response.result.value)
				
			} else {
				result("fail")
			}
		}
		
	}
	
	/*........................................................................*/
	
	/**
	Make a GET request on service
	
	- parameter url:     Url of internal service
	- parameter params:  Request Params
	- parameter headers: Request Headers
	- parameter result:  Result callback
	*/
	internal func getJsonResponse(url:String!, params:[String:AnyObject]?, headers:[String:String]?, result: @escaping (_ jsonObject:Any?) -> ()) {

		Alamofire.request(url, method: HTTPMethod.get, parameters: params, encoding: URLEncoding.default, headers: headers).responseJSON { (response) in
			
			if response.response?.statusCode == 200 {
				result(response.result.value as AnyObject)
				
			} else {
				result([])
			}
			
		}
		
	}
	
	/*........................................................................*/
	
	/**************************************************************************/
	
}
