//
//  CityTableViewCell.swift
//  E-Deploy
//
//  Created by Cristiano Douglas on 06/07/17.
//  Copyright © 2017 Cristiano Douglas. All rights reserved.
//

import UIKit

/******************************************************************************/
// MARK: CityTableViewCell -  UIViewController
/******************************************************************************/
class CityTableViewCell: UITableViewCell {
	
	/**************************************************************************/
	
	/*........................................................................*/
	// MARK: IBOutlet
	/*........................................................................*/

	@IBOutlet weak var lblNomeCidade: UILabel!
	@IBOutlet weak var lblNomeEstado: UILabel!
	@IBOutlet weak var viewColor: UIView!
	
	/*........................................................................*/
	// MARK: Var
	/*........................................................................*/
	var barColor : UIColor!
	
	var cidade : Cidade = Cidade() {
		
		didSet {
			self.configLabels()
		}
	}
	
	/**************************************************************************/
	
	/*........................................................................*/
	// MARK: Superclass functions
	/*........................................................................*/

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
		
		self.barColor = self.getRandomColor()
		self.viewColor.backgroundColor = self.barColor
    }
	
	/*........................................................................*/

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
	
	/*........................................................................*/
	
	func configLabels() {
		
		self.lblNomeCidade.text = cidade.Nome
		self.lblNomeEstado.text = cidade.Estado
		
	}
	
	/*........................................................................*/
	// MARK: Class functions
	/*........................................................................*/
	
	func getRandomColor() -> UIColor{
		
		let randomRed:CGFloat = CGFloat(drand48())
		
		let randomGreen:CGFloat = CGFloat(drand48())
		
		let randomBlue:CGFloat = CGFloat(drand48())
		
		return UIColor(red: randomRed, green: randomGreen, blue: randomBlue, alpha: 1.0)
		
	}
	
	/**************************************************************************/

}
