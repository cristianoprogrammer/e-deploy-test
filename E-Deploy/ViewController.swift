//
//  ViewController.swift
//  E-Deploy
//
//  Created by Cristiano Douglas on 05/07/17.
//  Copyright © 2017 Cristiano Douglas. All rights reserved.
//

import UIKit
import RNLoadingButton_Swift
import KVNProgress

/******************************************************************************/
// MARK: ViewController -  UIViewController
/******************************************************************************/
class ViewController: UIViewController {
	
	/**************************************************************************/
	
	/*........................................................................*/
	// MARK: IBOutlet
	/*........................................................................*/
	
	@IBOutlet weak var imgLogo		: UIImageView!
	
	@IBOutlet weak var lblCity		: UILabel!
	@IBOutlet weak var lblState		: UILabel!
	
	@IBOutlet weak var fieldCity	: UITextField!
	@IBOutlet weak var fieldState	: UITextField!
	
	@IBOutlet weak var btnFind		: RNLoadingButton!
	
	/*........................................................................*/
	// MARK: Var
	/*........................................................................*/
	
	var cityName	: String = ""
	var cityState	: String = ""
	var cities		: [Cidade] = []
	
	/**************************************************************************/
	
	/*........................................................................*/
	/* Superclass functions */
	/*........................................................................*/
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		
		self.fieldState.delegate	= self
		self.fieldCity.delegate		= self
		
		self.btnFind.hideTextWhenLoading = true
		self.btnFind.isLoading = false
		self.btnFind.activityIndicatorAlignment = RNActivityIndicatorAlignment.center
		self.btnFind.activityIndicatorEdgeInsets = UIEdgeInsets(top: 0, left: 50, bottom: 0, right: 10)
		self.btnFind.setTitleColor(UIColor(white: 1.000, alpha: 1.0), for: UIControlState.normal)
		self.btnFind.setTitle("BUSCAR", for: UIControlState.normal)
		
		self.setKeyboardNotification()
		
	}
	
	/*........................................................................*/
	
	override func viewWillAppear(_ animated: Bool) {
		
		self.title = "Buscar"
		
	}
	
	/*........................................................................*/

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	/*........................................................................*/
	// MARK: Class functions
	/*........................................................................*/
	
	func requestCities () {
		
		KVNProgress.show(withStatus: "Percorrendo o Brasil...")
		Facade.instance.requestCities {
			KVNProgress.dismiss()
			KVNProgress.showSuccess(withStatus: "Viagem completada!")
		}
	}
	
	/*........................................................................*/
	
	func closeKeyboad () {
		
		self.fieldCity.endEditing(true)
		self.fieldState.endEditing(true)
	}

	/*........................................................................*/
	// MARK: Action
	/*........................................................................*/
	
	@IBAction func actionFind(_ sender: Any) {
		
		self.cityName = self.fieldCity.text!
		self.cityState = self.fieldState.text!
		self.fieldCity.isEnabled = false
		self.fieldState.isEnabled = false
		self.btnFind.isLoading = true
		
		DispatchQueue.main.async {
			
			self.cities = Cidades.instance.findBy(self.cityName, and: self.cityState)
			//print(self.cities)
			self.fieldCity.isEnabled = true
			self.fieldState.isEnabled = true
			self.btnFind.isLoading = false
			self.performSegue(withIdentifier: "segueCities", sender: nil)
			
		}
		
		
	}
	
	/*........................................................................*/
	
	/**************************************************************************/
	
}

/******************************************************************************/
// MARK: ViewController -  UITextFieldDelegate
/******************************************************************************/
extension ViewController : UITextFieldDelegate {
	
	/*........................................................................*/
	// MARK: Keyboard Functions
	/*........................................................................*/
	
	func setKeyboardNotification () {
		NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(sender:)), name:NSNotification.Name.UIKeyboardWillShow, object: nil);
		NotificationCenter.default.addObserver(self, selector:#selector(self.keyboardWillHide(sender:)), name:NSNotification.Name.UIKeyboardWillHide, object: nil);
	}
	
	/*........................................................................*/
	
	func keyboardWillShow(sender: NSNotification) {
		self.view.frame.origin.y = -150
	}
	
	/*........................................................................*/
	
	func keyboardWillHide(sender: NSNotification) {
		self.view.frame.origin.y = 0
	}
	
	/*........................................................................*/
	// MARK: UITextFieldDelegate functions
	/*........................................................................*/
	
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		
		if (textField == self.fieldCity) {
			
			self.fieldState.becomeFirstResponder()
		} else {
			
			self.fieldState.resignFirstResponder()
			self.actionFind([])
		}
		return true;
	}
	
	/*........................................................................*/
	
}

