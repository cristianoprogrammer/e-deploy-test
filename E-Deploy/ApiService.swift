//
//  ApiService.swift
//  E-Deploy
//
//  Created by Cristiano Douglas on 05/07/17.
//  Copyright © 2017 Cristiano Douglas. All rights reserved.
//

import Foundation
import ObjectMapper
import Alamofire

/******************************************************************************/
// MARK: ApiService - BaseService
/******************************************************************************/

/// Api for all service request
class ApiService :  BaseService {
	
	/**************************************************************************/
	
	let URLCitiesList : String = "http://wsteste.devedp.com.br/Master/CidadeServico.svc/rest/BuscaTodasCidades"
	let URLCityScore : String = "http://wsteste.devedp.com.br/Master/CidadeServico.svc/rest/BuscaPontos"
	
	/**************************************************************************/
	
	/*........................................................................*/
	/* Shared Instance */
	
	static let instance = ApiService()
	static func getInstance() -> ApiService {
		return instance
	}
	
	/*........................................................................*/
	
	/**
	Get the cities list
	
	- parameter result: result closure
	- parameter fail: fail closure
	*/
	func citiesList(result: @escaping (_ cities:[Cidade]?) -> (), fail: @escaping (_ message:String?) -> ()) -> Void {
		
		self.getJsonResponse(url: self.URLCitiesList, params:nil, headers: nil) { (jsonObject) in
			
			
			if (jsonObject != nil) {
				
				let cidades = Mapper<Cidade>().mapArray(JSONArray: jsonObject as! [[String : Any]])
				result(cidades)
				
			} else {
				
				fail("Falha ao obter lista de cidades")
				
			}
		}
	}
	
	/*........................................................................*/
	
	/**
	Post a request for the city score
	
	- parameter params: params for request
	- parameter result: result closure
	- parameter fail: fail closure
	*/
	func cityScore(params:[String:Any]?, result: @escaping (_ score : String?) -> (), fail: @escaping (_ message:String?) -> ()) -> Void {
		
		self.postStringResponse(url: self.URLCityScore, params: params, headers: nil) { (score : String?) in
				
				if (score != "fail") {
					result(score)
				} else {
					fail("Falha ao obter pontuação")
				}
				
			}
		
	}
	
	/*........................................................................*/
	
	/**************************************************************************/
	
}
