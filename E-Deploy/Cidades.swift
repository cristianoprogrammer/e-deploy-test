//
//  Cidades.swift
//  E-Deploy
//
//  Created by Cristiano Douglas on 05/07/17.
//  Copyright © 2017 Cristiano Douglas. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

/******************************************************************************/
// MARK: Cidades
/******************************************************************************/

/// Controls and persists the list of cities on REAL
class Cidades {
	
	/**************************************************************************/
	
	/*........................................................................*/
	// MARK: VAR
	/*........................................................................*/
	
	var list : [Cidade] = [Cidade]()
	
	/**************************************************************************/
	
	/*........................................................................*/
	// MARK: Shared Instance
	/*........................................................................*/
	
	static let instance = Cidades()
	static func getInstance() -> Cidades {
		return instance
	}
	
	/*........................................................................*/
	// MARK: Class functions
	/*........................................................................*/
	
	/**
	Save the list of cities on REALM
	
	- parameter list: List of cities
	*/
	func save( list : [Cidade]) {
		
		let realm = try! Realm()
		try! realm.write {
			var id = 0
			for city in list {
				city.id = id
				realm.add(city,update: true)
				id += 1
			}
		}
		
	}
	
	/*........................................................................*/
	/**
	Find a city by name and state
	
	- parameter name: City name
	- parameter state: State name
	*/
	func findBy(_ name : String, and state : String) -> [Cidade] {
		
		let realm = try! Realm()
		let list = realm.objects(Cidade.self).filter("Nome contains[c] %@ AND Estado contains[c] %@", name, state) 
		print("Count: \(list.count)")
		
		self.list = Array(list)
		return self.list;
	}
	
	/*........................................................................*/
	
	/**************************************************************************/
	
}
