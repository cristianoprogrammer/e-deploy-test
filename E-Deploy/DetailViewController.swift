//
//  DetailViewController.swift
//  E-Deploy
//
//  Created by Cristiano Douglas on 06/07/17.
//  Copyright © 2017 Cristiano Douglas. All rights reserved.
//

import UIKit
import SAConfettiView
import KVNProgress

/******************************************************************************/
// MARK: DetailViewController -  UIViewController
/******************************************************************************/
class DetailViewController: UIViewController {

	/**************************************************************************/
	
	/*........................................................................*/
	// MARK: IBOutlet
	/*........................................................................*/
	
	@IBOutlet weak var lblCityName	: UILabel!
	@IBOutlet weak var lblCityScore	: UILabel!

	/*........................................................................*/
	// MARK: Var
	/*........................................................................*/
	
	var city		: Cidade = Cidade()
	var cityColor	: UIColor!
	var navColor	: UIColor!
	
	/**************************************************************************/
	
	/*........................................................................*/
	// MARK: Superclass functions
	/*........................................................................*/
	
    override func viewDidLoad() {
        super.viewDidLoad()

        self.lblCityName.text = self.city.Nome
		self.navigationController?.navigationBar.topItem?.title = ""
		self.navColor = self.navigationController?.navigationBar.barTintColor
		self.navigationController?.navigationBar.barTintColor = self.cityColor
	}
	
	/*........................................................................*/
	
	override func viewWillAppear(_ animated: Bool) {
		
		self.title = "Cidade"
		
		self.requestCityScore()
	}
	
	/*........................................................................*/
	
	override func viewWillDisappear(_ animated: Bool) {
		
		self.navigationController?.navigationBar.barTintColor = self.navColor

	}

	/*........................................................................*/
	
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
	
	/*........................................................................*/
	// MARK: Class functions
	/*........................................................................*/
	
	func requestCityScore() {
		
		var params : [String : Any] = [
			"Nome":self.city.Nome,
			"Estado":self.city.Estado
		]
		
		KVNProgress.show(withStatus: "Entrevistando cidade...")
		Facade.instance.requestCityScore(params: params, completed: { (score) in
			
			self.lblCityScore.text = "\((score)!)\nPontos"
			self.configConfettiStar()
			KVNProgress.dismiss()
			
		}) { (message) in
			
			KVNProgress.dismiss()
			KVNProgress.showError(withStatus: message)
		}
	}
	
	/*........................................................................*/
	
	func configConfettiStar() {
		
		let confettiView = SAConfettiView(frame: self.view.bounds)
		confettiView.type = .Star
		self.view.addSubview(confettiView)
		confettiView.startConfetti()
	}
	
	/*........................................................................*/

	/**************************************************************************/
}
